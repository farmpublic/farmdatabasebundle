<?php

namespace FarmPublic\DatabaseBundle\Api;

use GuzzleHttp\Exception\RequestException;
use PHPSupabase\Auth;
use PHPSupabase\Service;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class SupabaseApiClient
{
    private const ENDPOINT_AUTH = '/auth/v1/';

    public function __construct(
        private readonly string $apiUrl,
        #[\SensitiveParameter] private readonly string $apiKey,
    ) {
    }

    protected function getSupabaseService(string $endpoint): Service
    {
        return new Service($this->apiKey, $this->apiUrl.$endpoint);
    }

    protected function createAuth(): Auth
    {
        $service = $this->getSupabaseService(self::ENDPOINT_AUTH);

        return $service->createAuth();
    }

    public function getAuthUser(string $accessToken): array
    {
        $auth = $this->createAuth();

        try {
            $data = $auth->getUser($accessToken);
        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage(),
            ];
        }

        return (array) $data;
    }

    public function getAuthUserIdentifier(string $accessToken): string
    {
        $authUser = $this->getAuthUser($accessToken);

        if (isset($authUser['error']) || !isset($authUser['id'])) {
            throw new CustomUserMessageAuthenticationException($authUser['error'] ?? 'No user found');
        }

        return $authUser['id'];
    }

    /**
     * Générer le token d'accès à Supabase.
     */
    public function generateToken(string $email, string $password): array
    {
        $auth = $this->createAuth();

        try {
            $auth->signInWithEmailAndPassword($email, $password);
            $data = $auth->data();

            return [
                'token' => $data->access_token,
                'expires_in' => $data->expires_in,
                'expires_at' => $data->expires_at,
                'refresh_token' => $data->refresh_token,
                'user' => [
                    'id' => $data->user->id,
                    'email' => $data->user->email,
                    'last_sign_in_at' => $data->user->last_sign_in_at,
                ],
            ];
        } catch (RequestException $e) {
            return [
                'error' => $auth->getError(),
                'error_raw' => $e->getMessage(),
            ];
        }
    }

    /**
     * Générer le token d'accès à Supabase.
     */
    public function createUser(string $email, string $password, array $metadata = []): array
    {
        $auth = $this->createAuth();

        try {
            $auth->createUserWithEmailAndPassword($email, $password, $metadata);
            $data = $auth->data();

            return [
                'id' => $data->id,
                'email' => $data->email,
                'role' => $data->role,
                'created_at' => $data->created_at,
                'updated_at' => $data->updated_at,
                'app_metadata' => (array) $data->app_metadata,
                'user_metadata' => (array) $data->user_metadata,
                'confirmation_sent_at' => $data->confirmation_sent_at,
                'is_anonymous' => $data->is_anonymous,
            ];
        } catch (RequestException $e) {
            return [
                'error' => $auth->getError(),
                'error_raw' => $e->getMessage(),
            ];
        }
    }

    /**
     * Récupère toutes les données d'une table Supabase.
     *
     * @return array Données de la table ou tableau d'erreur
     */
    public function fetchAll(string $table, ?string $bearerToken = null): array
    {
        $service = $this->getSupabaseService(self::ENDPOINT_AUTH);
        if ($bearerToken) {
            $service->setBearerToken($bearerToken);
        }
        $db = $service->initializeDatabase($table, 'id');

        try {
            return $db->fetchAll()->getResult();
        } catch (RequestException $e) {
            return [
                'error' => $service->getError(),
                'error_raw' => $e->getMessage(),
            ];
        }
    }
}
