<?php

namespace FarmPublic\DatabaseBundle\Controller;

use FarmPublic\DatabaseBundle\Service\Webhook\WebhookKeyAuthenticatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AbstractWebhookController extends AbstractController
{
    public function __construct(
        private readonly WebhookKeyAuthenticatorService $webhookKeyAuthenticatorService,
    ) {
    }

    public function init(Request $request)
    {
        $webhookKey = $request->headers->get('X-Webhook-Key');

        if (!$this->webhookKeyAuthenticatorService->isWebhookKeyValid($webhookKey)) {
            return $this->json([
                'message' => 'Invalid or missing webhook key',
            ], Response::HTTP_UNAUTHORIZED);
        }
    }
}
