<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\EntityTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\UuidTrait;

trait CelluleTrait
{
    use EntityTrait;
    use UuidTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $codeCellule = null;

    #[ORM\Column]
    private ?int $capacite = null;

    #[ORM\Column]
    private ?bool $ventilable = null;

    #[ORM\Column]
    private ?bool $groupeFroid = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 15, scale: 3, nullable: true)]
    private ?string $nombreDebit = null;

    #[ORM\Column(nullable: true)]
    private ?int $ordre = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeCellule(): ?string
    {
        return $this->codeCellule;
    }

    public function setCodeCellule(string $codeCellule): self
    {
        $this->codeCellule = $codeCellule;

        return $this;
    }

    public function getCapacite(): ?int
    {
        return $this->capacite;
    }

    public function setCapacite(int $capacite): self
    {
        $this->capacite = $capacite;

        return $this;
    }

    public function isVentilable(): ?bool
    {
        return $this->ventilable;
    }

    public function setVentilable(bool $ventilable): self
    {
        $this->ventilable = $ventilable;

        return $this;
    }

    public function isGroupeFroid(): ?bool
    {
        return $this->groupeFroid;
    }

    public function setGroupeFroid(bool $groupeFroid): self
    {
        $this->groupeFroid = $groupeFroid;

        return $this;
    }

    public function getNombreDebit(): ?string
    {
        return $this->nombreDebit;
    }

    public function setNombreDebit(?string $nombreDebit): self
    {
        $this->nombreDebit = $nombreDebit;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }
}
