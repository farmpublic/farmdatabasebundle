<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\BaseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\BlameableTrait;

trait TypeRegroupementTrait
{
    use BaseTrait;
    use BlameableTrait;

    #[ORM\Column(length: 150)]
    private ?string $nom = null;

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
