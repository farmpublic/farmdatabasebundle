<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\AdresseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\BaseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\BlameableTrait;

trait OsTrait
{
    use AdresseTrait;
    use BaseTrait;
    use BlameableTrait;

    #[ORM\Column(length: 255)]
    private $nom;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $urlLogo = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $siret = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $tva = null;

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getUrlLogo(): ?string
    {
        return $this->urlLogo;
    }

    public function setUrlLogo(?string $urlLogo): self
    {
        $this->urlLogo = $urlLogo;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getTva(): ?string
    {
        return $this->tva;
    }

    public function setTva(?string $tva): self
    {
        $this->tva = $tva;

        return $this;
    }
}
