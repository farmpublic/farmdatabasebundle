<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\TimestampableTrait;

trait AppStatutTrait
{
    use TimestampableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true, options: ['comment' => 'Date de début du mode maintenance: si vide et actif = true, alors le site est en maintenance immédiatement'])]
    private ?\DateTimeImmutable $startAt = null;

    #[ORM\Column(nullable: true, options: ['comment' => 'Date de fin du mode maintenance: si vide et actif = true, alors le site est en maintenance sans date de fin'])]
    private ?\DateTimeImmutable $endAt = null;

    #[ORM\Column(options: ['comment' => 'Mode maintenance activé ?'])]
    private ?bool $active = null;

    #[ORM\Column(type: Types::TEXT, nullable: true, options: ['comment' => 'Message affiché en mode maintenance'])]
    private ?string $message = null;

    public function __toString(): string
    {
        return $this->message ?? '#'.$this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartAt(): ?\DateTimeImmutable
    {
        return $this->startAt;
    }

    public function setStartAt(?\DateTimeImmutable $startAt): static
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeImmutable
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeImmutable $endAt): static
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): static
    {
        $this->message = $message;

        return $this;
    }
}
