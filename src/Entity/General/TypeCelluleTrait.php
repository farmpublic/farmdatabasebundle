<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\EntityTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\UuidTrait;
use Symfony\Component\Serializer\Annotation\Groups;

trait TypeCelluleTrait
{
    use EntityTrait;
    use UuidTrait;

    #[Groups(['api:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(length: 255)]
    #[Groups(['api:read'])]
    private ?string $nom = null;

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }
}
