<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\AdresseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\EntityTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\GpsTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\TelephoneEmailTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\UuidTrait;
use Symfony\Component\Serializer\Annotation\Groups;

trait SiloTrait
{
    use EntityTrait;
    use GpsTrait;
    use AdresseTrait;
    use TelephoneEmailTrait;
    use UuidTrait;

    #[Groups(['api:read'])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(length: 255)]
    #[Groups(['api:read'])]
    private ?string $nom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $photoUrl = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $airtableRef;

    #[ORM\Column(nullable: true)]
    private ?bool $fictif = null;

    public function __construct()
    {
    }

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPhotoUrl(): ?string
    {
        return $this->photoUrl;
    }

    public function setPhotoUrl(?string $photoUrl): static
    {
        $this->photoUrl = $photoUrl;

        return $this;
    }

    public function getAirtableRef(): ?string
    {
        return $this->airtableRef;
    }

    public function setAirtableRef(?string $airtableRef): self
    {
        $this->airtableRef = $airtableRef;

        return $this;
    }
}
