<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\BaseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\BlameableTrait;

trait RegroupementTrait
{
    use BaseTrait;
    use BlameableTrait;

    #[ORM\Column(length: 150)]
    private ?string $nom = null;

    #[ORM\Column(length: 100)]
    private ?string $code = null;

    #[ORM\Column(length: 100, nullable: true)]
    private ?string $codeExterne = null;

    public function __toString(): string
    {
        return $this->nom ?? '';
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCodeExterne(): ?string
    {
        return $this->codeExterne;
    }

    public function setCodeExterne(?string $codeExterne): self
    {
        $this->codeExterne = $codeExterne;

        return $this;
    }
}
