<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\EntityTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\UuidTrait;

trait PeriodeStockageTrait
{
    use EntityTrait;
    use UuidTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }
}
