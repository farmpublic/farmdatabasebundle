<?php

namespace FarmPublic\DatabaseBundle\Entity\General;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\BaseTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\BlameableTrait;

trait ConverSiloTrait
{
    use BaseTrait;
    use BlameableTrait;

    #[ORM\Column(type: 'string', length: 255)]
    private $codeExterne;

    #[ORM\Column(nullable: true, options: ['comment' => 'Quantité hors OS'])]
    private ?bool $qteHorsOs = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeExterne(): ?string
    {
        return $this->codeExterne;
    }

    public function setCodeExterne(string $codeExterne): self
    {
        $this->codeExterne = $codeExterne;

        return $this;
    }

    public function isQteHorsOs(): ?bool
    {
        return $this->qteHorsOs;
    }

    public function setQteHorsOs(?bool $qteHorsOs): self
    {
        $this->qteHorsOs = $qteHorsOs;

        return $this;
    }
}
