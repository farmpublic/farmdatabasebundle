<?php

namespace FarmPublic\DatabaseBundle\Entity\Zone;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Entity\Traits\GpsTrait;
use FarmPublic\DatabaseBundle\Entity\Traits\TimestampableTrait;

trait FranceCommuneTrait
{
    use TimestampableTrait;
    use GpsTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    private ?string $nom = null;

    #[ORM\Column(type: 'integer')]
    private ?int $cp = null;

    #[ORM\Column(length: 150)]
    private ?string $acheminement = null;

    #[ORM\Column(length: 150, nullable: true)]
    private ?string $complement = null;

    #[ORM\Column(type: 'integer')]
    private ?int $codeInsee = null;

    public function __toString(): string
    {
        return $this->nom;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getCp(): ?int
    {
        return $this->cp;
    }

    public function setCp(?int $cp): static
    {
        $this->cp = $cp;

        return $this;
    }

    public function getAcheminement(): ?string
    {
        return $this->acheminement;
    }

    public function setAcheminement(?string $acheminement): static
    {
        $this->acheminement = $acheminement;

        return $this;
    }

    public function getComplement(): ?string
    {
        return $this->complement;
    }

    public function setComplement(?string $complement): static
    {
        $this->complement = $complement;

        return $this;
    }

    public function getCodeInsee(): ?int
    {
        return $this->codeInsee;
    }

    public function setCodeInsee(?int $codeInsee): static
    {
        $this->codeInsee = $codeInsee;

        return $this;
    }
}
