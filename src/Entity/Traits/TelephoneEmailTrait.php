<?php

namespace FarmPublic\DatabaseBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TelephoneEmailTrait
{
    #[ORM\Column(length: 20, nullable: true)]
    private ?string $telephoneFixe = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $telephonePortable = null;

    #[ORM\Column(length: 90, nullable: true)]
    private ?string $email = null;

    public function getTelephoneFixe(): ?string
    {
        return $this->telephoneFixe;
    }

    public function setTelephoneFixe(?string $telephoneFixe): static
    {
        $this->telephoneFixe = $telephoneFixe;

        return $this;
    }

    public function getTelephonePortable(): ?string
    {
        return $this->telephonePortable;
    }

    public function getTelephonePortableInternational(string $prefix = '+33'): ?string
    {
        if (null !== $this->telephonePortable && str_starts_with($this->telephonePortable, '0')) {
            return $prefix.substr($this->telephonePortable, 1);
        }

        return $this->telephonePortable;
    }

    public function setTelephonePortable(?string $telephonePortable): static
    {
        $this->telephonePortable = $telephonePortable;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): static
    {
        $this->email = $email;

        return $this;
    }
}
