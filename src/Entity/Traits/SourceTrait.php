<?php

namespace FarmPublic\DatabaseBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use FarmPublic\DatabaseBundle\Config\DataSource;

/**
 * Permet d'avoir les champs de la source de synchronisation.
 */
trait SourceTrait
{
    #[ORM\Column(length: 100, enumType: DataSource::class, nullable: true, options: ['comment' => 'Source de synchronisation'])]
    private ?DataSource $sourceFrom = null;

    #[ORM\Column(nullable: true, options: ['comment' => 'Date de synchronisation'])]
    private ?\DateTimeImmutable $sourceSyncAt = null;

    public function getSourceFrom(): ?DataSource
    {
        return $this->sourceFrom;
    }

    public function setSourceFrom(?DataSource $sourceFrom): static
    {
        $this->sourceFrom = $sourceFrom;

        return $this;
    }

    public function getSourceSyncAt(): ?\DateTimeImmutable
    {
        return $this->sourceSyncAt;
    }

    public function setSourceSyncAt(?\DateTimeImmutable $sourceSyncAt): static
    {
        $this->sourceSyncAt = $sourceSyncAt;

        return $this;
    }

    public function setSourceSyncNow(): static
    {
        $this->sourceSyncAt = new \DateTimeImmutable();

        return $this;
    }
}
