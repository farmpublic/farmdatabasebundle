<?php

namespace FarmPublic\DatabaseBundle\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait BlameableTrait
{
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $createdBy;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $updatedBy;

    public function getCreatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): ?string
    {
        return $this->createdBy;
    }

    public function setUpdatedBy(?string $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }
}
