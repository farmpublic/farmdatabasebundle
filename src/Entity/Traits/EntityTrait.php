<?php

namespace FarmPublic\DatabaseBundle\Entity\Traits;

/**
 * @deprecated Utiliser les traits BaseTrait à la place
 */
trait EntityTrait
{
    use TimestampableTrait;
    use BlameableTrait;
}
