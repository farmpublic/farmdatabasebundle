<?php

namespace FarmPublic\DatabaseBundle\Entity\System;

interface LogInterface
{
    public function hoursCreatedAgo(): ?int;

    public function getId(): ?int;

    public function getMessage(): ?string;

    public function setMessage(string $message): BaseLog;

    public function getContext(): array;

    public function setContext(array $context): BaseLog;

    public function getLevel(): ?int;

    public function setLevel(int $level): BaseLog;

    public function getExtra(): array;

    public function setExtra(array $extra): BaseLog;

    public function getChannel(): ?string;

    public function setChannel(string $channel): BaseLog;

    public function getLevelName(): ?string;

    public function setLevelName(string $levelName): BaseLog;

    public function getCreatedAt(): ?\DateTimeImmutable;

    public function setCreatedAt(\DateTimeImmutable $createdAt): BaseLog;
}
