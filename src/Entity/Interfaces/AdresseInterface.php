<?php

namespace FarmPublic\DatabaseBundle\Entity\Interfaces;

interface AdresseInterface
{
    public function getAdresse(): ?string;

    public function setAdresse(?string $adresse): static;

    public function getVille(): ?string;

    public function setVille(?string $ville): static;

    public function getCp(): ?string;

    public function setCp(?string $cp): static;

    public function getPays(): ?string;

    public function setPays(?string $pays): static;
}
