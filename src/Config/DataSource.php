<?php

namespace FarmPublic\DatabaseBundle\Config;

/**
 * Source de données possibles.
 */
enum DataSource: string
{
    case BACKEND_ADMIN = 'Backend admin';
    case BACKEND_API = 'Backend API';
    case BACKEND_FORM = 'Backend formulaire';
    case BACKEND_SYNC = 'Backend synchro';
    case CSV = 'CSV';
}
