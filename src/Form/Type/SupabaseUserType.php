<?php

namespace FarmPublic\DatabaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class SupabaseUserType extends AbstractType
{
    private const int PASSWORD_MIN_LENGTH = 6;
    private const string PASSWORD_REGEX_MESSAGE = 'Le mot de passe doit contenir au moins un caractère de chaque type : minuscule, majuscule, chiffre et caractère spécial (!@#$%^&*()_+-=[]{};\':"|<>?,./`~)';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'L\'email est requis']),
                    new Email(['message' => 'L\'email n\'est pas valide']),
                ],
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe',
                'constraints' => [
                    new NotBlank(['message' => 'Le mot de passe est requis']),
                    new Length([
                        'min' => self::PASSWORD_MIN_LENGTH,
                        'minMessage' => 'Le mot de passe doit faire au moins {{ limit }} caractères',
                    ]),
                    // regex imposé par Supabase :
                    new Regex([
                        'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+\-=\[\]{};\':"|<>?,\.\/`~])[A-Za-z\d!@#$%^&*()_+\-=\[\]{};\':"|<>?,\.\/`~]{6,}$/',
                        'message' => self::PASSWORD_REGEX_MESSAGE,
                    ]),
                ],
                'help' => self::PASSWORD_REGEX_MESSAGE.' ('.self::PASSWORD_MIN_LENGTH.' caractères minimum)',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Créer l\'utilisateur',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ])
        ;
    }
}
