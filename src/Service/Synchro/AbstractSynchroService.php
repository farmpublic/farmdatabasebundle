<?php

declare(strict_types=1);

namespace FarmPublic\DatabaseBundle\Service\Synchro;

use Doctrine\ORM\EntityManagerInterface;
use FarmPublic\DatabaseBundle\Api\SupabaseApiClient;
use FarmPublic\DatabaseBundle\Config\DataSource;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Uid\Uuid;

abstract class AbstractSynchroService
{
    public ?SymfonyStyle $io = null;
    protected ?string $supabaseToken = null;

    public function __construct(
        protected SupabaseApiClient $supabaseApiClient,
        protected EntityManagerInterface $em,
    ) {
        $this->io = new SymfonyStyle(new ArgvInput(), new ConsoleOutput());
    }

    public function synchronize(): void
    {
        $this->io->title(sprintf('Synchronisation des données sur la table %s', $this->getTableName()));

        $result = $this->fetchDataFromSupabase();
        if (null === $result) {
            return;
        }

        [$created, $updated] = $this->processData($result);

        $this->em->flush();

        $this->displaySyncResults($created, $updated);
    }

    protected function fetchDataFromSupabase(): ?array
    {
        // Si requete avec jointure, on ajoute la jointure dans la requete
        $customQuery = $this->getCustomQuery();
        if ($customQuery) {
            $query = $customQuery;
        } else {
            $query = $this->getTableName();
        }

        $result = $this->supabaseApiClient->fetchAll($query, $this->supabaseToken);

        if (isset($result['error'])) {
            $this->io->error($result['error']);
            $this->io->note($result['error_raw'] ?? 'Pas de raw');

            return null;
        }

        return $result;
    }

    protected function displaySyncResults(int $created, int $updated): void
    {
        $this->io->success(sprintf(
            'Synchronisation terminée : %d éléments créés, %d éléments mis à jour',
            $created,
            $updated
        ));
    }

    /**
     * Récupère l'objet ou la valeur de l'UUID de l'objet ou de l'array.
     */
    protected function getUuidFromData(object|array $data): ?Uuid
    {
        $uuid = null;
        if (is_object($data) && isset($data->uuid)) {
            $uuid = $data->uuid;
        }

        if (is_array($data) && isset($data['uuid'])) {
            $uuid = $data['uuid'];
        }

        return null !== $uuid ? Uuid::fromString($uuid) : null;
    }

    /**
     * Permet de mettre à jour les champs de tracking d'une entité.
     */
    protected function updateTrackingEntity(object $entity, object $data): void
    {
        // si $entity a le trait SourceTrait
        if (method_exists($entity, 'setSourceFrom')) {
            $entity->setSourceFrom(DataSource::BACKEND_SYNC);
        }

        if (method_exists($entity, 'setSourceSyncNow')) {
            $entity->setSourceSyncNow();
        }

        // si $entity a le trait BleamableTrait
        if (method_exists($entity, 'setCreatedBy')) {
            $entity->setCreatedBy($data->created_by);
        }
        if (method_exists($entity, 'setUpdatedBy')) {
            $entity->setUpdatedBy($data->updated_by);
        }
    }

    /**
     * Retourne la requête personnalisée à utiliser pour la synchronisation avec Supabase.
     *
     * @return string|null La requête personnalisée ou null si aucune requête personnalisée n'est nécessaire
     */
    protected function getCustomQuery(): ?string
    {
        return null;
    }

    /**
     * Retourne le nom de la table distance à synchroniser.
     */
    abstract protected function getTableName(): string;

    abstract protected function processData(array $result): array;
}
