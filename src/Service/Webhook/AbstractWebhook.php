<?php

namespace FarmPublic\DatabaseBundle\Service\Webhook;

/**
 * Classe abstraite pour la gestion des webhooks Supabase.
 */
abstract class AbstractWebhook
{
    /**
     * Méthode qui écoute les webhooks Supabase et dispatche en fonction du type de mouvement en base de données.
     */
    public function listener(string $content): string
    {
        $content = json_decode($content, true);

        $function = strtolower($content['type']);

        $this->$function($content);

        return $this->makeResponse();
    }

    /**
     * Méthode qui fabrique une réponse pour le webhook.
     */
    public function makeResponse(): string
    {
        return 'OK, webhook key is valid, and the webhook is working';
    }

    abstract public function update(array $content): void;

    abstract public function delete(array $content): void;

    abstract public function insert(array $content): void;
}
