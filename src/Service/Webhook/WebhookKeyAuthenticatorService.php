<?php

namespace FarmPublic\DatabaseBundle\Service\Webhook;

class WebhookKeyAuthenticatorService
{
    public function __construct(
        private string $webhookKey,
    ) {
    }

    public function isWebhookKeyValid(?string $webhookKey = null): bool
    {
        if (!$webhookKey) {
            return false;
        }

        return $webhookKey === $this->webhookKey;
    }
}
