<?php

namespace FarmPublic\DatabaseBundle\Tool;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JsonResponseTool
{
    /**
     * Création d'une réponse Erreur en json.
     */
    public static function newError(string $message, int $code = Response::HTTP_BAD_REQUEST, ?array $detail = null): JsonResponse
    {
        return new JsonResponse([
            'errorMessage' => $message,
            'errorCode' => $code,
            'errorDetail' => $detail,
        ],
            $code);
    }

    /**
     * Création d'une réponse Erreur d'API Externe en json.
     */
    public static function newErrorApi(string $message, int $code = Response::HTTP_BAD_REQUEST, ?array $detail = null): JsonResponse
    {
        return new JsonResponse([
            'errorMessage' => 'Problème API Externe : '.$message,
            'errorApiCode' => $code,
            'errorApiDetail' => $detail,
        ], $code);
    }
}
