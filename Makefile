#---Symfony-And-Docker-Makefile---------------#
# Author: https://github.com/yoanbernabeu
# License: MIT
#---------------------------------------------#

#---VARIABLES---------------------------------#
#---DOCKER---#
DOCKER = docker
DOCKER_RUN = $(DOCKER) run
DOCKER_COMPOSE = docker compose
DOCKER_COMPOSE_UP = $(DOCKER_COMPOSE) up -d
DOCKER_COMPOSE_STOP = $(DOCKER_COMPOSE) stop
#------------#

#---COMPOSER-#
COMPOSER = composer
COMPOSER_INSTALL = $(COMPOSER) install
COMPOSER_UPDATE = $(COMPOSER) update
#------------#

#---PHPQA---#
PHPQA = jakzal/phpqa:php8.3
PHPQA_RUN = $(DOCKER_RUN) --init --rm -v $(PWD):/project -w /project $(PHPQA)
#------------#

#---PHPUNIT-#
PHPUNIT = APP_ENV=test $(SYMFONY) php bin/phpunit
#------------#
#---------------------------------------------#

## === 🆘  HELP ==================================================
help: ## Show this help.
	@echo "Symfony-And-Docker-Makefile"
	@echo "---------------------------"
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#---------------------------------------------#

## === 🐋  DOCKER ================================================
docker-up: ## Start docker containers.
	$(DOCKER_COMPOSE_UP)
.PHONY: docker-up

docker-stop: ## Stop docker containers.
	$(DOCKER_COMPOSE_STOP)
.PHONY: docker-stop

docker-build: ## Build docker containers.
	$(DOCKER) build . -f ./docker/Dockerfile -t farmpilot:latest
.PHONY: docker-build

docker-run-without-async: ## Run docker containers without async consumer.
	$(DOCKER) run -it -e DISABLE_ASYNC=true farmpilot:latest -p 8080:80
.PHONY: docker-run-without-async

docker-run: ## Run docker containers.
	$(DOCKER) run -it -e DISABLE_ASYNC=false farmpilot:latest -p 8080:80
.PHONY: docker-run
#---------------------------------------------#


## === 🐛  PHPQA =================================================
qa-cs-fixer-dry-run: ## Run php-cs-fixer in dry-run mode.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose --dry-run
.PHONY: qa-cs-fixer-dry-run

qa-cs-fixer: ## Run php-cs-fixer.
	$(PHPQA_RUN) php-cs-fixer fix ./src --rules=@Symfony --verbose
.PHONY: qa-cs-fixer

qa-phpstan: ## Run phpstan.
	$(PHPQA_RUN) phpstan analyse ./src --level=3
.PHONY: qa-phpstan

qa-phpcpd: ## Run phpcpd (copy/paste detector).
	$(PHPQA_RUN) phpcpd ./src
.PHONY: qa-phpcpd

qa-phpdd: ## Run phpdd (dead code detector).
	$(PHPQA_RUN) phpdd ./src
.PHONY: qa-phpdd

qa-php-metrics: ## Run php-metrics.
	$(PHPQA_RUN) phpmetrics --report-html=var/phpmetrics ./src
.PHONY: qa-php-metrics
#---------------------------------------------#


## === 🔎  TESTS 🔥 —————————————————————————————————————————————————————————————————— =================================================
tests: ## Run tests.
	$(PHPUNIT) --testdox
.PHONY: tests

tests-coverage: ## Run tests with coverage and filter on className
	XDEBUG_MODE=coverage $(PHPUNIT) --coverage-html var/coverage --filter ClassName
.PHONY: tests-coverage
#---------------------------------------------#


## === ⭐  OTHER =================================================
before-commit: ## Run before commit.
	$(MAKE) qa-cs-fixer qa-phpstan
.PHONY: before-commit
#---------------------------------------------#