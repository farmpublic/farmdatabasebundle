# FarmDatabaseBundle

## Installation

Installer le bundle via Composer :
`composer require farmpublic/farmdatabasebundle`

## Structure

Voici la structure du dossier `.src\`

* API : Client des API utilisés
* DTO: 
    * General
    * Dashdoc
* Entity
    * Traits : tout ce qui est réutilisable par les autres
        * SourceTrait : Permet de gérer la source de l'entité
        * BaseTrait : Permet de gérer la base de nos entités (id, timestampable, uuid)
    * Interfaces : les interfaces des entités
    * Auth : User, Rôles, Droits, Groupe
    * General : Infos générales comme les états et OS
    * Produit
        * Cereal : Infos sur les céréales
        * Appro : Infos sur les appros
    * Silo : Infos sur les silos
    * System : Log
    * Zone : Infos sur les zones
    * Tiers
        * Exploitation
        * Transporteur
        * Banque
        * Fournisseur
* Listener : déclenchements des évènements
* Service : Services avec services
* Tool : Fonctions statiques

## utilisation et configuration des API

### Supabase

Pour utiliser l'API Supabase, il faut configurer votre service dans le fichier `config/services.yaml`:

```yaml
services:
    FarmPublic\DatabaseBundle\Api\SupabaseApiClient:
        arguments:
            $apiUrl: '%env(API_SUPABASE_URL)%'
            $apiKey: '%env(API_SUPABASE_KEY)%'
```

Il est possible aussi d'utiliser avec l'attribut `Autowire` :

```php
#[Autowire('%env(API_SUPABASE_URL)%')] string $apiUrl,
#[Autowire('%env(API_SUPABASE_KEY)%')] string $apiKey,
```


Ensuite vous pouvez utiliser les fonctions de la classe `SupabaseApiClient` pour interagir avec l'API Supabase.

## ❤️ Contribuer au projet

### 📜 Règles de codage

- Application des règles de [code standard Symfony](https://symfony.com/doc/current/contributing/code/standards.html)
- Application des standards [PSR-1, PSR-2, PSR-4 et PSR-12](https://www.php-fig.org/psr/)
- Indentation = 4 espaces

### 🔥 Merge Request

- Les "Merges Requests" sont **RECOMMANDÉS**, et la relecture de code entre pairs et vivement recommandé.

- Pour les changements majeurs, veuillez en discuter avec le reste de l'équipe.

### ☑️ Tests et pipeline

- Veuillez vous assurer de mettre à jour les tests le cas échéant ou d'en écrire de nouveaux pour couvrir vos ajouts.

- Veuillez également vous assurer que vos modifications passent le pipeline d'intégration continue.

- Pour lancer les tests localement, veuillez lancer la commande suivante (Nécessite Makefile).

```bash
make tests
```

- Pour lancer l'évaluation de la couverture de code, veuillez lancer la commande suivante :

```bash 
make coverage
```

- Le rapport de couverture de code est disponible dans le repertoire *./var/coverage/*

### Gestion des versions (tags)

Nous utilisons [Semantic Versioning](https://semver.org/) pour la gestion des versions.
Pour publier une nouvelle version, il suffit de créer un tag de la forme `x.y.z` depuis l'interface GitLab (en ce basant sur la branche `main`).

### 🧑🏾‍🤝‍🧑🏼 Code de Conduite

Dans l'intérêt de favoriser un environnement ouvert et accueillant, nous nous engageons à faire de la participation à ce projet une expérience exempte de harcèlement pour tout le monde, quel que soit le niveau d'expérience, le sexe, l'identité ou l'expression de genre, l'orientation sexuelle, le handicap, l'apparence personnelle, la taille physique, l'origine ethnique, l'âge, la religion ou la nationalité.